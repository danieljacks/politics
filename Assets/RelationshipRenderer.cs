﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class RelationshipRenderer : MonoBehaviour {

	public LineRenderer renderer;
	public Transform startT;
	public Transform endT;
	public float relationship = 0.0f; //0..1 range
	public float lineWidth = 0.4f;
	public float offsetWidth = 0.15f;
	public float arrowHeadPercent = 0.2f; //Whole arrow thing is not working quite as expected, but it looks cool anyway, so I think ill keep it...

	void Start () {
		renderer = GetComponent<LineRenderer> ();
		Color lineColor = Color.Lerp(Color.red, Color.green, (relationship + 1) * 0.5f);
		lineColor = MakeGreyscale (lineColor, 1 - Mathf.Abs(relationship));
		renderer.startColor = lineColor;
		renderer.endColor = lineColor;
		renderer.material = new Material (Shader.Find("UI/Default"));
		renderer.widthMultiplier = lineWidth;
		renderer.widthCurve = new AnimationCurve(new Keyframe[] {
			new Keyframe(0, 0.4f),
            new Keyframe(0.999f - arrowHeadPercent, 0.4f),  // neck of arrow
            new Keyframe(1 - arrowHeadPercent, 1f),  // max width of arrow head
            new Keyframe(1, 0f)
		});
	}

	private Color MakeGreyscale (Color color, float grayness) {
		float h,s,v;
		Color.RGBToHSV (color, out h, out s, out v);
		s *= 1-grayness;
		v *= 1 - (grayness * 0.5f);

		return Color.HSVToRGB (h, s, v);
	}

	void OnGUI () {
		Vector2 startPos = startT.position;
		Vector2 endPos = endT.position;

		Vector2 diff = endPos - startPos;
		Vector2 offset = Vector2.Perpendicular(diff);
		offset.Normalize();
		offset *= offsetWidth;

		startPos += offset;
		endPos += offset;

		renderer.SetPositions(new Vector3[] {
			startPos,
			Vector3.Lerp(startPos, endPos, 0.999f - arrowHeadPercent),
			Vector3.Lerp(startPos, endPos, 1 - arrowHeadPercent),
			endPos });
	}
}
