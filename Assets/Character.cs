﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

	public int id = 0;
	public bool isTarget = false;
	public float aggressiveness = 0.0f;


	private Dictionary<Character, float> relationships = new Dictionary<Character, float>(); //floats representing relationship levels, 0..1 range
	private Dictionary<Character, RelationshipRenderer> relationshipLines = new Dictionary<Character, RelationshipRenderer> (); //Physical lines depicting relationsips


	void Start () {
		GenerateRandomRelationships();
	}

	private void GenerateRandomRelationships () {
		foreach (Character character in GameObject.FindObjectsOfType<Character>()) {
			float relationshipValue = Random.Range(-1, 2);
			SetRelationship (character, relationshipValue);
		}
	}

	private void CheckRelationshipLines () {
		// ensure there is an associated line for each relationship
		foreach (KeyValuePair<Character, float> relationship in relationships) {
			if (!relationshipLines.ContainsKey(relationship.Key)) { 
				RelationshipRenderer line = CreateLineRenderer(relationship.Key);
				relationshipLines.Add(relationship.Key, line);
			}
		}

		// ensure there are no line without associated relationships
		foreach (KeyValuePair<Character, RelationshipRenderer> relationshipLine in relationshipLines) {
			if (!relationships.ContainsKey(relationshipLine.Key)) {
				relationshipLines.Remove(relationshipLine.Key);
			}
		}
	}

	private RelationshipRenderer CreateLineRenderer (Character target) {
		if (!relationships.ContainsKey(target)) {
			Debug.LogError ("Something is missing from dictionary");
			return null;
		}

		string name = "LineRendererParent: " + id.ToString();
		var parentObj = GameObject.Find(name);
		if (parentObj == null) {
			parentObj = new GameObject (name);
		}

		var lineObj = new GameObject (id.ToString() + "-" + target.id.ToString());
		lineObj.transform.parent = parentObj.transform;
		var renderer = lineObj.AddComponent<RelationshipRenderer> ();
		renderer.startT = transform;
		renderer.endT = target.transform;
		renderer.relationship = GetRelationship(target);

		return renderer;
	}

	/// <summary>
	/// Returns the realtionship with a character.
	/// </summary>
	/// <param name="character">The target of the relationship</param>
	/// <returns>returns a float in the 0..1 range</returns>
	public float GetRelationship (Character target) {
		return relationships[target];
	}

	private void SetRelationship (Character target, float realtionshipValue) {
		realtionshipValue = Mathf.Clamp (realtionshipValue, -1.0f, 1.0f);
		relationships[target] = realtionshipValue;
		CheckRelationshipLines ();
	}
}
